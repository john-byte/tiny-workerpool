package workerpool

import "sync"

type WorkerTask func()

type WorkerPool struct {
	tasks      chan WorkerTask
	tasksLock  *sync.Mutex
	workersCnt uint64
}

func New(workersCnt uint64, capacityFactor uint64) *WorkerPool {
	return &WorkerPool{
		tasks:      make(chan WorkerTask, workersCnt*capacityFactor),
		workersCnt: workersCnt,
		tasksLock:  &sync.Mutex{},
	}
}

func (self *WorkerPool) Init() {
	for i := 0; i < int(self.workersCnt); i += 1 {
		go func() {
			for {
				self.tasksLock.Lock()
				task := <-self.tasks
				self.tasksLock.Unlock()
				task()
			}
		}()
	}
}

func (self *WorkerPool) EnqueueTask(enqTask WorkerTask) {
	self.tasks <- enqTask
}
